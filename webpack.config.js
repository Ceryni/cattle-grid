const path = require('path');
const nodeExternals = require('webpack-node-externals');
const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const fs = require('fs');

module.exports = {
    // mode: 'development',
    optimization: {
        minimizer: [new TerserJSPlugin({}), new OptimizeCssAssetsPlugin({})],
    },
    entry: {
        main: ['./assets/scss/style.scss'],
    },
    // js output path
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name]/base.min.js',
    },
    target: 'node',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader?name=/img/[name].[ext]',
                        // image output path
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'src/',

                            //HERE
                            publicPath: '../src/',
                        },
                    },
                ],
            },
            {
                test: /\.(css|sass|scss)$/,
                exclude: /assets\/fonts/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                    },
                    'sass-loader',
                ],
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(woff(2)?|ttf|woff|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader?limit=100000',
                        // font output path
                        options: {
                            name: 'fonts/[name].[ext]',
                            mimetype: 'application/font-woff',
                            publicPath: '../',
                        },
                    },
                ],
            },
            {
                test: /\.modernizrrc.js$/,
                use: ['modernizr-loader'],
            },
            {
                test: /\.modernizrrc(\.json)?$/,
                use: ['modernizr-loader', 'json-loader'],
            },
        ],
    },
    watch: true,
    watchOptions: {
        ignored: ['node_modules'],
    },
    resolve: {
        alias: {
            modernizr$: path.resolve(__dirname, '.modernizrrc'),
        },
    },
    plugins: [
        new CleanWebpackPlugin(),
        // css output path
        new MiniCssExtractPlugin({
            filename: '[name]/style.min.css',
            path: path.resolve(__dirname, 'dist/css'),
            chunkFilename: '[id].css',
        }),
    ],
};
