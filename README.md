![Cattle Grid](https://usecattlegrid.com/assets/images/logo.png)

Extremely lightweight, basic flex grid built from a simple mixin

## Demo

[Cattle Grid - Extremely Lightweight, Basic Flex CSS Grid Built From Simple Mixins](https://usecattlegrid.com)   


## Getting Started

Finally got around to building my own Simple CSS Framework after becoming too annoyed at the reliance on the likes of a Foundation & Bootstrap. Foundation served me well for years, but it’s too bloated and too contaminated with elements that could be refactored and simplified. Which leads me to this! I now have my simple (under 80kb) responsive flex box grid + components which I’ve named the Cattle Grid

If you want just the css grid, grab [_grid.scss](https://gitlab.com/Ceryni/cattle-grid/blob/master/assets/scss/base/_grid.scss) from assets/scss/base (under 25kb) and import it into your stylesheets! The repo for the entire codebase can be found over on [Gitlab](https://gitlab.com/Ceryni/cattle-grid)  and add this to your own project! Simple.

If you wish to use the entire project, the folder structure is very simple.

#### Base
General contains all files needed to set up a new site, from colours, helpers, form elements and mixins

#### Site
Site contains files related to the project you're about to build.


### Set up
Cattle Grid runs on Webpack which is contained within the folder. To get up and running, first ensure that you have node.js installed on your machine. First run _npm install_ to bring down all of the node modules. To get the watcher to compile on save, run _npm run watch__

---

### Defining Initial Variables

```
// base variables
$grid-gutter-small: $medium-space !default;
$grid-gutter-width: 30px;
$grid-gutter-medium: ($grid-gutter-width / 2);
$grid-max-width: $global-width + ($grid-gutter-medium * 2) !default;
$grid-columns: 12 !default;
// set your breakpoints
$breakpoint-small: 767px !default;
$breakpoint-medium: 992px !default;
$breakpoint-large: 1140px !default;
$breakpoint-xlarge: 1440px !default;


$global-width: $breakpoint-large;

// set your queries
$small : '(min-width: 0)';
$small-only : '(max-width: ' + $breakpoint-small + ')';
$medium: '(min-width: ' + ($breakpoint-small + 1) + ')';
$medium-only: '(min-width: ' + $breakpoint-small + ') and (max-width: ' + ($breakpoint-medium - 1) + ')';
$medium-down: '(max-width: ' + ($breakpoint-medium) + ')';
$large: '(min-width: ' + $breakpoint-medium + ')';
$large-only: '(min-width: ' + $breakpoint-medium + ') and (max-width: ' + ($breakpoint-xlarge - 1) + ')';
$large-down: '(max-width: ' + $breakpoint-large + ')';
$xlarge: '(min-width: ' + $breakpoint-xlarge + ')';
$xlarge-down: '(max-width: ' + $breakpoint-xlarge + ')';
$xlarge-up: '(min-width: ' + ($breakpoint-large + 1) + ')';

// map your points
$breakpoints: (
	'small' $small,
	'medium' $medium,
	'large' $large,
	'xlarge' $xlarge,
) !default;
```

### Using Breakpoint Mixin

```
@mixin breakpoint($media) {
	@media #{$media} {
		@content;
	}
}
```

### Using Grid Markup

```
<div class="container">
	<div class="row">
		<div class="small-12 medium-6 large-3 columns">column</div>
		<div class="small-12 medium-6 large-3 columns">column</div>
		<div class="small-12 medium-6 large-3 columns">column</div>
		<div class="small-12 medium-6 large-3 columns">column</div>
	</div>
</div>
```

---

## Extra mixins & Settings

These are just tidbits that come in handy when starting out a new build!


### Sizings
```
$small-space: 5px;
$medium-space: 20px;
$large-space: 40px;
$xlarge-space: 60px;
$hundred: ($large-space + $xlarge-space);
$input-height: $large-space;
$border-radius: 3px;
```


### Text Alignments
To be able to align text left, right or centrally within the markup, there are 4 classes available!

_To use_
```
<div class="small-text-center medium-text-right large-text-left xlarge-text-center">Align me!</div>
```

### Atom Users!

If you use Atom - this handy snippet allows for easy autofill of sizes! Be a faster dev

```
'.source.css.scss':
  'rem':
    'prefix': 'rem'
    'body': 'rem-calc($0);'
  '5px':
    'prefix': '5px'
    'body': '$small-space';
  '-5px':
    'prefix': '-5px'
    'body': '-$small-space';
  '10px':
    'prefix': '10px'
    'body': '($small-space * 2)';
  '-10px':
    'prefix': '-10px'
    'body': '-($small-space * 2)';
  '15px':
    'prefix': '15px'
    'body': '($small-space * 3)';
  '-15px':
    'prefix': '-15px'
    'body': '-($small-space * 3)';
  '20px':
    'prefix': '20px'
    'body': '$medium-space';
  '-20px':
    'prefix': '-20px'
    'body': '-$medium-space';
  '25px':
    'prefix': '25px'
    'body': '($small-space * 5)';
  '-25px':
    'prefix': '-25px'
    'body': '-($small-space * 5)';
  '30px':
    'prefix': '30px'
    'body': '($xlarge-space / 2)';
  '-30px':
    'prefix': '-30px'
    'body': '-($xlarge-space / 2)';
  '35px':
    'prefix': '35px'
    'body': '($large-space - $small-space)';
  '-35px':
    'prefix': '-35px'
    'body': '-($large-space - $small-space)';
  '40px':
    'prefix': '40px'
    'body': '$large-space';
  '-40px':
    'prefix': '-40px'
    'body': '-$large-space';
  '45px':
    'prefix': '45px'
    'body': '($large-space + $small-space)';
  '-45px':
    'prefix': '-45px'
    'body': '-($large-space + $small-space)';
  '50px':
    'prefix': '50px'
    'body': '($small-space * 10)';
  '-50px':
    'prefix': '-50px'
    'body': '-($small-space * 10)';
  '60px':
    'prefix': '60px'
    'body': '$xlarge-space';
  '-60px':
    'prefix': '-60px'
    'body': '-$xlarge-space';
  '70px':
    'prefix': '70px'
    'body': '($xlarge-space + ($small-space * 2))';
  '-70px':
    'prefix': '-70px'
    'body': '-($xlarge-space + ($small-space * 2))';
  '80px':
    'prefix': '80px'
    'body': '($large-space * 2)';
  '-80px':
    'prefix': '-80px'
    'body': '-($large-space * 2)';
  '90px':
    'prefix': '90px'
    'body': '(($large-space * 2) + ($small-space * 2))';
  '-90px':
    'prefix': '-90px'
    'body': '-(($large-space * 2) + ($small-space * 2))';
  'hover':
    'prefix': 'hover'
    'body': '&:hover,\n' +
	   '&:focus, \n' +
	   '&:active {\n' +
	   '$0\n' +
	   '}';
  'Small Query':
    'prefix': 'small'
    'body': '@media #{$small-only}{\n' +
	   '$0\n' +
	   '}';
  'Medium Query':
    'prefix': 'med'
    'body': '@media #{$medium}{\n' +
	   '$0\n' +
	   '}';
'.text.html.php':
  'Create Row & Column F6 Structure':
    'prefix': 'row'
    'body': '<div class="row align-middle align-center">\n' +
  				'<div class="col-12 col-md-12 col-lg-12">\n' +
  		   			'$0\n' +
  				'</div>\n' +
  			'</div>';
  'Create Row & Column Bootatrap Structure':
    'prefix': 'cont'
    'body': '<div class="container">\n' +
                '<div class="row align-middle align-center">\n' +
      				'<div class="col-12 col-md-12 col-lg-12">\n' +
      		   			'$0\n' +
      				'</div>\n' +
      			'</div>\n' +
            '</div>';
  'Var Dump':
    'prefix': 'var_'
    'body': 'var_dump($0)';
'.source.js':
  'Console Log':
    'prefix': 'console'
    'body': 'console.log("$0", )';
 'Comment':
   'prefix': 'comment'
   'body': '// -----------------\n' +
	   '// $0\n' +
	   '// -----------------\n';
```


### Crayola

```
$palette: (
	primary: #1C3C49,
	primaryText: #ffffff,
	secondary: #DDCEB1,
	secondaryText: #1C3C49,
	tertiary: #C19A37,
	tertiaryText: #1C3C49,
	quaternary: #518793,
	quaternaryText: #1C3C49,
	success: #4caf50,
	error: #f44336,
	alert: #ff9800,
	warning: #00bcd4,
	light-grey: #c4c5c5,
	dark-grey: #222222
);

// shorthand
$primary: map-get($palette, primary);
$primaryText: map-get($palette, primaryText);
$secondary: map-get($palette, secondary);
$secondaryText: map-get($palette, secondaryText);
$tertiary: map-get($palette, tertiary);
$tertiaryText: map-get($palette, tertiaryText);
$quaternary: map-get($palette, quaternary);
$quaternaryText: map-get($palette, quaternaryText);
$light: map-get($palette, light-grey);
$dark: map-get($palette, dark-grey);
$border: darken($lightGrey, 5%);

// notifications
$success: map-get($palette, success);
$error: map-get($palette, error);
$alert: map-get($palette, alert);
$warning: map-get($palette, warning);

// social
$facebook: #3b5998;
$twitter: #55acee;
$instagram: #e1306c;
$pinterest: #bd081c;
$etsy: #d5641c;
$linkedin: #0077b5;
$buffer: #323b43;
$reddit: #ff4500;
$email: $primary;

// colours
$body-background: linear-gradient(90deg,#111A23,#{$primary});
$body-font-color: $secondary;
$anchor-font-color: $secondary;
```

_To use_

```
background-color: $primary;
color: $primaryText;
```

### Maps

```
//map networks
$config: (
	shades: (
		light: (
			colour: $light,
			text: $dark,
		),
		dark: (
			colour: $dark,
			text: $light,
		),
	),
	notifications: (
		success: (
			colour: $success,
			text: #ffffff,
		),
		error: (
			colour: $error,
			text: #ffffff,
		),
		alert: (
			colour: $alert,
			text: #ffffff,
		),
		warning: (
			colour: $warning,
			text: #ffffff,
		),
	),
	themes: (
		primary: (
			colour: $primary,
			text: $primaryText
		),
		secondary: (
			colour: $secondary,
			text: $secondaryText,
		),
		tertiary: (
			colour: $tertiary,
			text: $tertiaryText,
		),
		quaternary: (
			colour: $quaternary,
			text: $quaternaryText,
		),
	),
	networks: (
		facebook: (
			colour: $facebook,
			text: #ffffff,
		),
		twitter: (
			colour: $twitter,
			text: #ffffff,
		),
		instagram: (
			colour: $instagram,
			text: #ffffff,
		),
		pinterest: (
			colour: $pinterest,
			text: #ffffff,
		),
		etsy: (
			colour: $etsy,
			text: #ffffff,
		),
		linkedin: (
			colour: $linkedin,
			text: #ffffff,
		),
		buffer: (
			colour: $buffer,
			text: #ffffff,
		),
		reddit: (
			colour: $reddit,
			text: #ffffff,
		),
		email: (
			colour: $primary,
			text: $body-font-color,
		)
	),
);

```

_To use_

```
// map through the themes
	@each $theme in map-keys(map-get($config, themes)) {
		&.#{$theme} {
			@include button-bg(map-fetch($config, themes $theme colour ), map-fetch($config, themes $theme text ));
			&.hollow {
				@include button-hollow(map-fetch($config, themes $theme colour ), map-fetch($config, themes $theme colour ));
			}
		}
	}
	// map through the networks
	@each $network in map-keys(map-get($config, networks)) {
		&.#{$network} {
			@include button-bg(map-fetch($config, networks $network colour ), map-fetch($config, networks $network text ));
			&.hollow {
				@include button-hollow(map-fetch($config, networks $network colour ), map-fetch($config, networks $network colour ));
			}
		}
	}
	// map through the notifications
	@each $notify in map-keys(map-get($config, notifications)) {
		&.#{$notify} {
			@include button-bg(map-fetch($config, notifications $notify colour ), map-fetch($config, notifications $notify text ));
			&.hollow {
				@include button-hollow(map-fetch($config, notifications $notify colour ), map-fetch($config, notifications $notify colour ));
			}
		}
	}
	// map through the shades
	@each $shade in map-keys(map-get($config, shades)) {
		&.#{$shade} {
			@include button-bg(map-fetch($config, shades $shade colour ), map-fetch($config, shades $shade text ));
			&.hollow {
				@include button-hollow(map-fetch($config, shades $shade colour ), map-fetch($config, shades $shade colour ));
			}
		}
	}
```

### Buttons

```
@mixin button-bg($bg, $txt) {
	background: $bg;
	border: $bg;
	color: $txt;
	&:hover,
	&:focus,
	&:active {
		background:darken($bg,10%);
		border:darken($bg, 10%);
		transition: all 0.3s ease;
	}
}

```

_To use:_

```
@include button-bg(#000000, #ffffff);
```

### Hollow Buttons

```
@mixin button-hollow($border, $txt) {
	background: transparent;
	border: 1px solid $border;
	color: $txt;
	&:hover,
	&:focus,
	&:active {
		border: 1px solid darken($border, 10%);
		transition: all 0.3s ease;
		color: darken($txt, 10%);
	}
}
```

_To use:_

```
@include button-hollow(#000000, #000000);
```

### Pseudos

```
@mixin pseudo($display: block, $pos: absolute, $content: ''){
	content: $content;
	display: $display;
	position: $pos;
}
```

_To use:_

```
@include pseudo;
```

### Placeholders

```
@mixin input-placeholder {
	&.placeholder { @content; }
	&:-moz-placeholder { @content; }
	&::-moz-placeholder { @content; }
	&:-ms-input-placeholder { @content; }
	&::-webkit-input-placeholder { @content; }
}
```

_To use:_

```
@include input-placeholder {
	color: $grey;
}
```

### Headlines

```
@mixin heading {
	color: $body-font-color;
	font-weight: 300;
	font-style: normal;
	text-rendering: optimizeLegibility;
	margin-top: 0;
	margin-bottom: ($small-space * 3);
	line-height: normal;
	font-family: $header-font-family;
	padding: 0;
	text-transform: uppercase;
	letter-spacing: 2px;
}

@mixin h1 {
	@include heading;
	font-size: $headline1;
	@media #{$small-only}{
		font-size: rem-calc(30);
	}
}
```

_To use:_

```
@include h1;
```


### Font sizes

```
// Rem Calc
@function rem-calc($font-size) {
	$rem-size: $font-size / 16;
	@return #{$rem-size}rem;
}

```

_To use:_

```
font-size: rem-calc(12)
```


### Z Indexes

```
$z-index: (
	modal: 200,
	navigation: 100,
	footer: 10,
	header: 50,
	main: 10,
);

@function z-index($key) {
	@return map-get($z-index, $key);
}
@mixin z-index($key) {
	z-index: z-index($key);
}
```

_To use:_

```
@include z-index(header);
```

### Fades

```
@mixin fade($type) {
	@if $type == 'hide' {
		visibility: hidden;
		opacity: 0;
		transition: visibility 1s, opacity 1s;
	}
	@else if $type == 'show' {
		visibility: visible;
		opacity: 1;
		transition: visibility 1s, opacity 1s;
	}
}
```

_To use:_

```
@include fade(hide)
@include fade(show)
```


---


## Extends

### Alignment & Positioning

```
// align texts center
.text-center {
	text-align: center;
}
// .. left
.text-left {
	text-align: left;
}
// .. right
.text-right {
	text-align: right;
}
// Columns in a flex grid can be aligned horizontal or vertical inside of their parent.
// within a container, align items visually center
.align-all {
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	text-align: center;
}
.align-stretch {
	display: flex;
	align-items: stretch;
}
.align-middle {
	display: flex;
	align-items: center;
}
.align-bottom {
	display: flex;
	align-items: flex-end;
}
.align-top {
	display: flex;
	align-items: flex-start;
}
.align-center {
	display: flex;
	justify-content: center;
}
.align-left {
	display: flex;
    justify-content: flex-start;
}
.align-right {
	display: flex;
    justify-content: flex-end;
}
.align-center {
	display: flex;
    justify-content: center;
}
.align-justify {
	display: flex;
    -webkit-box-pack: justify;
    justify-content: space-between;
}
.align-spaced {
	display: flex;
    justify-content: space-around;
}
// Columns in a flex grid can be aligned horizontal or vertical individually also
// align a column within a row to the bottom
.align-self-bottom {
	align-self: flex-end;
}
// align a column within a row to the middle
.align-self-middle {
	align-self: center;
}
// align a column within a row to the top
.align-self-top {
	align-self: flex-start;
}
// Elements can be absolutely positioned inside of it's relative parent
// Horizontally centers the element
.horizontal-center {
	position: absolute;
	left: 50%;
	top: auto;
	transform: translateX(-50%);
}
// Vertically centers the element
.vertical-center {
	position: absolute;
	left: auto;
	top: 50%;
	transform: translateY(-50%);
}
// Abolutely centers the element
.absolute-center {
	position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}

```

_To use:_

```
@extend .align-all;
@extend .horizontal-center;
@extend .vertical-center;
@extend .absolute-center;

<div class="container">
	<div class="row align-all">
		<div class="small-12 medium-6 columns align-self-top">column</div>
		<div class="small-12 medium-6 columns align-self-middle"> column</div>
		<div class="small-12 medium-6 columns align-self-bottom">column</div>
	</div>
	<div class="row">
		<div class="small-12 medium-4 columns align-self-top">
			align-self-top
		</div>
		<div class="small-12 medium-4 columns align-self-middle">
			align-self-middle
		</div>
		<div class="small-12 medium-4 columns align-self-bottom">
			align-self-bottom
		</div>
	</div>
	<div>
		<div class="small-12 columns">
			<div class="box horizontal-center align-all">
				horizontal-center
			</div>
			<div class="box vertical-center align-all">
				vertical-center
			</div>
			<div class="box absolute-center align-all">
				absolute-center
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-4 columns">
			<div class="box box-inside align-all">
				<div class="box-inner">
					<p>
						align-all
					</p>
				</div>
			</div>
		</div>
		<div class="small-12 medium-4 columns">
			<div class="box box-inside align-top">
				<div class="box-inner">
					<p>
						align-top
					</p>
				</div>
			</div>
		</div>
		<div class="small-12 medium-4 columns">
			<div class="box box-inside align-bottom">
				<div class="box-inner">
					<p>
						align-bottom
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
```

---

## Authors

* **Ceryni** - *Initial work* - [Ceryni173](https://github.com/Ceryni173)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspired by Bootstrap markup
